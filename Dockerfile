FROM php:8.2-fpm

# Update/Install dependencies
RUN apt-get update && apt-get install -y git unzip zip libicu-dev libzip-dev libpq-dev librabbitmq-dev libssl-dev xdg-utils

# Setup php extensions
RUN docker-php-ext-install intl zip opcache pdo pdo_pgsql bcmath
RUN docker-php-ext-enable opcache 
RUN pecl install amqp && docker-php-ext-enable amqp

# Disable short open tag
RUN echo "short_open_tag=off" >> /usr/local/etc/php/php.ini 

# Setup OPcache
RUN echo "opcache.enable=1" >> /usr/local/etc/php/php.ini 
RUN echo "opcache.memory_consumption=256" >> /usr/local/etc/php/php.ini 
RUN echo "opcache.max_accelerated_files=20000" >> /usr/local/etc/php/php.ini

# Install composer
COPY --from=composer:2.7 /usr/bin/composer /usr/local/bin/composer

# Install symfony CLI
RUN curl -sS https://get.symfony.com/cli/installer | bash 
RUN mv /root/.symfony*/bin/symfony /usr/local/bin/symfony

# Setup user
RUN useradd -ms /bin/bash usuario
USER usuario

WORKDIR /var/www/html
COPY --chown=usuario:usuario . /var/www/html

RUN composer install

EXPOSE 8000

CMD ["symfony", "server:start"]
