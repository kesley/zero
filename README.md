# Zero

## Tecnologias

- PHP 8.2
- Docker
- Symfony 7
- RabbitMq

## Configuração Inicial
Copia as variaveis de ambiente já configuradas para a aplicação dockerizada
```sh
cp .env.example .env
```

Monta a imagem do projeto e sobe os containers
```sh
docker compose up -d --build
```

Executa as migrações do banco de dados
```sh
docker exec zero-app bin/console d:m:m
```

## Credenciais

- **Database**
  - path: `localhost:5432`
  - type: `postgresql`
  - user: `user`
  - password: `postgres`
  - databse_name: `app`
- **RabbitMq**
  - path: `http://localhost:15672`
  - user: `guest`
  - password: `guest`

## Endpoints

URl base `http://127.0.0.1:8000`

### Usuários

##### [GET] /users
Lista todos os usuários cadastrados
```sh
http://127.0.0.1:8000/users
```

##### [POST] /users
Realiza o cadastro de um usuário

```sh
http://127.0.0.1:8000/users
```

```sh
# Body
{
  "name": "User",
  "document": "11122233344",
  "type": 2, 
  "email": "user@email.com",
  "password": "123456"
}
```
### Transferências

##### [GET] /transfers

todas as transferências
```sh
http://127.0.0.1:8000/transfers
```

filtrando por usuário
```sh
http://127.0.0.1:8000/transfers?user=1 
```

##### [POST] /transfers
Realiza o cadastro de uma transferência

```sh
http://127.0.0.1:8000/transfers
```
```sh
# Body
{
  "value": 15.35,
  "payer": 2,
  "payee": 1
}
```

### Carteiras

##### [GET] /wallets
Lista todas as carteiras registradas

```sh
http://127.0.0.1:8000/wallets 
```

##### [GET] /wallets/:id

Retorna os dados de uma carteira
```sh
http://127.0.0.1:8000/wallets 
```
