<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240619232343 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transfers ADD payee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transfers ADD payer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transfers ADD CONSTRAINT FK_802A3918CB4B68F FOREIGN KEY (payee_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transfers ADD CONSTRAINT FK_802A3918C17AD9A9 FOREIGN KEY (payer_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_802A3918CB4B68F ON transfers (payee_id)');
        $this->addSql('CREATE INDEX IDX_802A3918C17AD9A9 ON transfers (payer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE transfers DROP CONSTRAINT FK_802A3918CB4B68F');
        $this->addSql('ALTER TABLE transfers DROP CONSTRAINT FK_802A3918C17AD9A9');
        $this->addSql('DROP INDEX IDX_802A3918CB4B68F');
        $this->addSql('DROP INDEX IDX_802A3918C17AD9A9');
        $this->addSql('ALTER TABLE transfers DROP payee_id');
        $this->addSql('ALTER TABLE transfers DROP payer_id');
    }
}
