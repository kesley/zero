<?php

namespace App\Enum;

enum UserType: int
{
    case Person = 1;
    case Store = 2;

    public function canBePayer(): bool
    {
        return match ($this) {
            UserType::Person => true,
            UserType::Store => false
        };
    }
}
