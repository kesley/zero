<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findWithWallet(int $userId): ?User
    {
        $qb = $this->createQueryBuilder('u');

        $qb->leftJoin('u.wallet', 'w')->addSelect('w');
        $qb->where("u.id = $userId");

        return $qb->getQuery()->getOneOrNullResult();
    }
}
