<?php

namespace App\Repository;

use App\Entity\Transfer;
use App\Filter\TransferFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Transfer>
 */
class TransferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transfer::class);
    }

    public function findAllFiltered(TransferFilter $filter)
    {
        $qb = $this->createQueryBuilder('t');

        if ($filter->getUserId()) {
            $qb->where('t.payer = :user');
            $qb->orWhere('t.payee = :user');

            $qb->setParameter('user', $filter->getUserId());
        }

        return $qb->getQuery()->getResult();
    }
}
