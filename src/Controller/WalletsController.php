<?php

namespace App\Controller;

use App\Entity\Wallet;
use App\Repository\WalletRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/wallets', name: 'wallets')]
class WalletsController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer
    ) {}

    #[Route('', name: '.index', methods: 'GET')]
    public function index(WalletRepository $walletRepository): JsonResponse
    {
        $wallets = $walletRepository->findAll();

        return new JsonResponse(
            data: $this->serializer->serialize($wallets, 'json'),
            json: true
        );
    }

    #[Route('/{id}', name: '.show', methods: 'GET')]
    public function show(Wallet $wallet): JsonResponse
    {
        return new JsonResponse(
            data: $this->serializer->serialize($wallet, 'json'),
            json: true
        );
    }
}
