<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/users', name: 'users')]
class UsersController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer
    ) {}

    #[Route('', name: '.index', methods: 'GET')]
    public function index(UserRepository $userRepository): JsonResponse
    {
        $users = $userRepository->findAll();

        return new JsonResponse(
            data: $this->serializer->serialize($users, 'json'),
            json: true
        );
    }

    #[Route('', name: '.create', methods: 'POST')]
    public function create(Request $request, ValidatorInterface $validator, EntityManagerInterface $em): JsonResponse
    {
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json');

        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return new JsonResponse(
                status: 422,
                data: $this->serializer->serialize($errors, 'json'),
                json: true
            );
        }

        $em->persist($user);
        $em->flush();

        return new JsonResponse(
            status: 201,
            data: $this->serializer->serialize($user, 'json'),
            json: true
        );
    }
}
