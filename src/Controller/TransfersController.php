<?php

namespace App\Controller;

use App\Entity\Transfer;
use App\Filter\TransferFilter;
use App\Message\TransferNotification;
use App\Repository\TransferRepository;
use App\Repository\UserRepository;
use App\Service\MakeTransferService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/transfers', name: 'transfers')]
class TransfersController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
    ) {}

    #[Route('', name: '.index', methods: 'GET')]
    public function index(Request $request, TransferRepository $transferRepository): JsonResponse
    {
        $filter = new TransferFilter($request->query);
        $transfers = $transferRepository->findAllFiltered($filter);

        return new JsonResponse(
            data: $this->serializer->serialize($transfers, 'json'),
            json: true
        );
    }

    #[Route('', name: '.create', methods: 'POST')]
    public function create(
        Request $request,
        UserRepository $userRepository,
        ValidatorInterface $validator,
        MakeTransferService $makeTransfer,
        MessageBusInterface $bus
    ): JsonResponse {
        $postData = json_decode($request->getContent());

        $transfer = new Transfer();
        $transfer
            ->setValue($postData->value)
            ->setPayer($userRepository->findWithWallet($postData->payer))
            ->setPayee($userRepository->findWithWallet($postData->payee));

        $errors = $validator->validate($transfer);
        if (count($errors) > 0) {
            return new JsonResponse(
                status: 422,
                data: $this->serializer->serialize($errors, 'json'),
                json: true
            );
        }

        if (false === $makeTransfer->call($transfer)) {
            return new JsonResponse(
                status: 401,
                data: $makeTransfer->getError(),
            );
        }

        $bus->dispatch(new TransferNotification($transfer));

        return new JsonResponse(
            status: 201,
            data: $this->serializer->serialize($transfer, 'json'),
            json: true
        );
    }
}
