<?php

namespace App\Filter;

use Symfony\Component\HttpFoundation\InputBag;

class TransferFilter
{
    private ?int $userId = null;

    public function __construct(InputBag $query)
    {
        $this->userId = filter_var($query->get('user', null), FILTER_SANITIZE_NUMBER_INT) ?: null;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
