<?php

namespace App\Message;

use App\Entity\Transfer;

final class TransferNotification
{
    private string $email;
    private string $message;

    public function __construct(Transfer $transfer)
    {
        $this->email = $transfer->getPayee()->getEmail();
        $this->message = "Você recebeu um pagamento de R\$ {$transfer->getValue()}";
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
