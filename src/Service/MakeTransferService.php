<?php

namespace App\Service;

use App\Entity\Transfer;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

class MakeTransferService
{
    private ?array $error = null;

    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepository,
    ) {}

    public function getError(): ?array
    {
        return $this->error;
    }

    public function call(Transfer $transfer): bool
    {
        if (false === $this->validate($transfer)) {
            return false;
        }

        return $this->transfer($transfer);
    }

    private function validate(Transfer $transfer): bool
    {
        if ($transfer->getPayer() === $transfer->getPayee()) {
            $this->error = [
                'message' => 'Payer e Payee não podem ser iguais',
                'details' => 'Um usuário não pode transferir para ele mesmo.'
            ];

            return false;
        }

        if (false === $transfer->getPayer()->getType()->canBePayer()) {
            $this->error = [
                'message' => 'Este usuário não pode realizar pagamentos.',
                'details' => 'Usuários do tipo Loja, não podem realizar pagamentos.'
            ];

            return false;
        }

        if (false === $transfer->getPayer()->getWallet()->hasEnoughBalance($transfer->getValue())) {
            $this->error = [
                'message' => 'Saldo insuficiente',
                'details' => 'O usuario precisa ter saldo suficiente para realizar pagamentos.'
            ];

            return false;
        }

        return true;
    }

    private function authorize(): void
    {
        $client = new Client();
        $client->get('https://util.devi.tools/api/v2/authorize');
    }

    private function transfer(Transfer $transfer): bool
    {
        $payerWallet = $transfer->getPayer()->getWallet();
        $payeeWallet = $transfer->getPayee()->getWallet();

        $this->em->beginTransaction();

        try {
            $payerWallet->removeBalance($transfer->getValue());
            $payeeWallet->addBalance($transfer->getValue());

            $this->em->persist($transfer);
            $this->em->persist($payerWallet);
            $this->em->persist($payeeWallet);

            $this->authorize();

            $this->em->flush();
            $this->em->commit();

            return true;
        } catch (ClientException $exception) {
            $this->em->rollback();

            $this->error = [
                'message' => 'Não Autorizado.',
                'details' => 'Esta transação não foi autorizada.'
            ];

            return false;
        } catch (\Exception $exception) {
            $this->em->rollback();

            $this->error = [
                'message' => 'Ocorreu um problema ao realizar a transação',
                'details' => $exception->getMessage()
            ];

            return false;
        }
    }
}
