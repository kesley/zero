<?php

namespace App\Entity;

use App\Enum\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity('email')]
#[UniqueEntity('document')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'users')]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $document = null;

    #[Assert\NotBlank]
    #[ORM\Column(type: Types::SMALLINT, enumType: UserType::class)]
    private ?UserType $type = null;

    #[Assert\Email]
    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $email = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[Ignore]
    #[ORM\OneToMany(targetEntity: Transfer::class, mappedBy: 'payer')]
    private Collection $payments;

    #[Ignore]
    #[ORM\OneToMany(targetEntity: Transfer::class, mappedBy: 'payee')]
    private Collection $receipts;

    #[Ignore]
    #[ORM\OneToOne(targetEntity: Wallet::class, inversedBy: 'user', cascade: ['persist'])]
    #[ORM\JoinColumn(name: 'wallet_id', referencedColumnName: 'id')]
    private Wallet|null $wallet = null;

    public function __construct()
    {
        if (null === $this->wallet) {
            $this->wallet = new Wallet();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function setDocument(string $document): static
    {
        $this->document = $document;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);

        return $this;
    }

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function getType(): ?UserType
    {
        return $this->type;
    }

    public function setType(UserType $type): static
    {
        $this->type = $type;

        return $this;
    }
}
