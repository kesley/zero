<?php

namespace App\Entity;

use App\Repository\WalletRepository;
use App\Trait\MonetaryConversionTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: WalletRepository::class)]
#[ORM\Table(name: 'wallets')]
class Wallet
{
    use MonetaryConversionTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?int $balance = 0;

    #[Ignore]
    #[ORM\OneToOne(targetEntity: User::class, mappedBy: 'wallet')]
    private User|null $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBalance(): ?float
    {
        return $this->convertCentsToDecimal($this->balance);
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Verifica se o usuário saldo para realizar uma transferência
     *
     * @param float $value - Valor da transferência
     *
     * @return bool
     */
    public function hasEnoughBalance(float $value): bool
    {
        return $this->balance >= $this->convertDecimalToCents($value);
    }

    /**
     * Adiciona valor ao saldo da carteira
     *
     * @param float $value - Valor da transferência
     */
    public function addBalance(float $value): static
    {
        $this->balance += $this->convertDecimalToCents($value);

        return $this;
    }

    /**
     * Remove saldo do usuário
     *
     * @param float $value - Valor da transferência
     */
    public function removeBalance(float $value): static
    {
        $this->balance -= $this->convertDecimalToCents($value);

        return $this;
    }
}
