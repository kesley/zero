<?php

namespace App\Entity;

use App\Repository\TransferRepository;
use App\Trait\MonetaryConversionTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TransferRepository::class)]
#[ORM\Table(name: 'transfers')]
class Transfer
{
    use MonetaryConversionTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\GreaterThan(0)]
    #[ORM\Column(type: Types::BIGINT)]
    private ?int $value = null;

    #[Assert\NotNull]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'receipts')]
    #[ORM\JoinColumn(name: 'payee_id', referencedColumnName: 'id')]
    private ?User $payee;

    #[Assert\NotNull]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(name: 'payer_id', referencedColumnName: 'id')]
    private ?User $payer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?float
    {
        return $this->convertCentsToDecimal($this->value);
    }

    public function setValue(float $value): static
    {
        $this->value = $this->convertDecimalToCents($value);

        return $this;
    }

    public function getPayee(): ?User
    {
        return $this->payee;
    }

    public function setPayee(?User $payee): static
    {
        $this->payee = $payee;

        return $this;
    }

    public function getPayer(): ?User
    {
        return $this->payer;
    }

    public function setPayer(?User $payer): static
    {
        $this->payer = $payer;

        return $this;
    }
}
