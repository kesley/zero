<?php

namespace App\Trait;

trait MonetaryConversionTrait
{
    /**
     * Converte um valor decimal para centavos
     *
     * @param float $value - Valor a ser convertido
     *
     * @return int
     */
    public function convertDecimalToCents(float $value): int
    {
        return (int) bcmul($value, 100);
    }

    /**
     * Converte um valor decimal para centavos
     *
     * @param int $value - Valor a ser convertido
     *
     * @return float
     */
    public function convertCentsToDecimal(int $value): float
    {
        return $value / 100;
    }
}
