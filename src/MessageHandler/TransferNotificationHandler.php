<?php

namespace App\MessageHandler;

use App\Message\TransferNotification;
use GuzzleHttp\Client;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class TransferNotificationHandler
{
    public function __invoke(TransferNotification $notification): void
    {
        $client = new Client();
        $client->post('https://util.devi.tools/api/v1/notify', [
            'email' => $notification->getEmail(),
            'message' => $notification->getMessage()
        ]);
    }
}
